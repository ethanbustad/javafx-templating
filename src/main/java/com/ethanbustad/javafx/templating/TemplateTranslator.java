/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating;

import com.ethanbustad.javafx.templating.elements.NodeFactory;

import java.util.List;

import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;

import org.json.JSONArray;
import org.json.JSONObject;

public class TemplateTranslator {

	public static enum Field {
		ELEMENTS;

		public String value() {
			return toString().toLowerCase();
		}

	}

	public static Region getSceneContent(JSONObject template) {
		GridPane gridPane = getGridPane(12, 12);

		List<Node> children = gridPane.getChildren();

		JSONArray elements = template.getJSONArray(Field.ELEMENTS.value());

		for (int i = 0; i < elements.length(); i++) {
			JSONObject element = elements.getJSONObject(i);

			children.add(NodeFactory.getNode(element));
		}

		return gridPane;
	}

	protected static GridPane getGridPane(int cols, int rows) {
		GridPane gridPane = new GridPane();

		List<ColumnConstraints> columnConstraints =
			gridPane.getColumnConstraints();

		for (int col = 0; col < cols; col++) {
			ColumnConstraints cc = new ColumnConstraints();

			cc.setPercentWidth(100.0 / cols);

			columnConstraints.add(cc);
		}

		List<RowConstraints> rowConstraints =
			gridPane.getRowConstraints();

		for (int row = 0; row < rows; row++) {
			RowConstraints rc = new RowConstraints();

			rc.setPercentHeight(100.0 / rows);

			rowConstraints.add(rc);
		}

		return gridPane;
	}

}
