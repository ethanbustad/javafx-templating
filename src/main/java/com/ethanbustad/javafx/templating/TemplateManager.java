/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating;

import com.ethanbustad.javafx.templating.util.JsonUtil;
import com.ethanbustad.javafx.templating.util.Logger;

import java.io.InputStream;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class TemplateManager {

	public JSONObject getTemplate(String id) {
		return templates.get(id);
	}

	public void init() {
		Map<String, JSONObject> templates = new HashMap<>();

		JSONObject templateFiles;

		try {
			templateFiles = getJsonObject(
				BASE_TEMPLATE_PATH + TEMPLATE_INDEX_FILE);
		}
		catch (Exception e) {
			Logger.log("Failed to load template index file");

			throw new RuntimeException(e);
		}

		for (String id : templateFiles.keySet()) {
			String templateFilename = templateFiles.getString(id);

			try {
				templates.put(
					(String)id,
					getJsonObject(BASE_TEMPLATE_PATH + templateFilename));
			}
			catch (Exception e) {
				Logger.log(
					"Failed to load template for file " + templateFilename, e);
			}
		}

		this.templates = templates;
	}

	protected JSONObject getJsonObject(String filename) throws Exception {
		ClassLoader classLoader = this.getClass().getClassLoader();

		InputStream in = classLoader.getResourceAsStream(filename);

		return JsonUtil.getJsonObject(in);
	}

	protected Map<String, JSONObject> templates;

	private static final String BASE_TEMPLATE_PATH =
		"com/ethanbustad/javafx/templating/";
	private static final String TEMPLATE_INDEX_FILE =
		"template-files.json";

}
