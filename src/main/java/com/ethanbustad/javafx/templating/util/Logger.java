/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

public class Logger {

	public static void debug(String message) {
		log(message, null, 2, "DEBUG");
	}

	public static void log(String message) {
		log(message, null, 2, null);
	}

	public static void log(String message, Throwable cause) {
		log(message, cause, 2, null);
	}

	public static void log(
		String message, Throwable cause, int index, String extra) {

		StringBuilder sb = new StringBuilder();

		sb.append(df.format(new Date()));
		sb.append(SEPARATOR);

		if ((extra != null) && !extra.isEmpty()) {
			sb.append(extra);
			sb.append(SEPARATOR);
		}

		try {
			throw new Exception();
		}
		catch (Exception e) {
			StackTraceElement[] elements = e.getStackTrace();

			if (elements.length > index) {
				String className = elements[index].getClassName();

				sb.append(className.substring(className.lastIndexOf(".") + 1));

				sb.append(".");
				sb.append(elements[index].getMethodName());
				sb.append("(L");
				sb.append(elements[index].getLineNumber());
				sb.append(")");
				sb.append(SEPARATOR);
			}
		}

		sb.append(message);

		System.out.println(sb.toString());

		if (cause != null) {
			cause.printStackTrace();
		}
	}

	protected static final String SEPARATOR = "||";

	protected static DateFormat df = new SimpleDateFormat(
		"MMM dd, HH:mm:ss.SSSZ");

}
