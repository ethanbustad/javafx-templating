/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.actions;

import com.ethanbustad.javafx.templating.Application;
import com.ethanbustad.javafx.templating.ImpossibleException;
import com.ethanbustad.javafx.templating.util.Logger;

import javafx.event.EventHandler;

import org.json.JSONArray;
import org.json.JSONObject;

public class EventHandlerFactory {

	public static enum Field {
		TYPE, VALUE;

		public String value() {
			return toString().toLowerCase();
		}

	}

	public static enum Type {
		DEBUG, NAVIGATION;
	}

	public static EventHandler getEventHandler(JSONObject template) {
		Type type = null;

		String typeString = template.optString(Field.TYPE.value());

		try {
			type = Type.valueOf(typeString.toUpperCase());
		}
		catch (IllegalArgumentException|NullPointerException e) {
			Logger.log("Event handler type \"" + type + "\" is invalid.");

			return null;
		}

		if (type.equals(Type.DEBUG)) {
			String message = template.getString(Field.VALUE.value());

			return (event) -> Logger.debug(message);
		}
		else if (type.equals(Type.NAVIGATION)) {
			String location = template.getString(Field.VALUE.value());

			return (event) -> Application.navigate(location);
		}
		else {
			throw new ImpossibleException("I must have missed a conditional");
		}
	}

	public static EventHandler getEventHandler(Object template) {
		if (template == null) {
			return null;
		}
		else if (template instanceof JSONArray) {
			JSONArray templateJSONArray = (JSONArray)template;

			CompositeEventHandler compositeEventHandler =
				new CompositeEventHandler();

			for (int i = 0; i < templateJSONArray.length(); i++) {
				compositeEventHandler.add(
					getEventHandler(templateJSONArray.getJSONObject(i)));
			}

			return compositeEventHandler;
		}
		else if (template instanceof JSONObject) {
			return getEventHandler((JSONObject)template);
		}
		else {
			throw new ImpossibleException(
				"\"template\" can only be of the types: null, JSONArray, or " +
					"JSONObject");
		}
	}

}
