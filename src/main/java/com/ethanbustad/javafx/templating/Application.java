/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating;

import com.ethanbustad.javafx.templating.TemplateManager;
import com.ethanbustad.javafx.templating.TemplateTranslator;

import com.ethanbustad.javafx.templating.util.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.Region;

import javafx.stage.Stage;

import org.json.JSONObject;

public class Application extends javafx.application.Application {

	public static final int APP_HEIGHT = 600;
	public static final String APP_TITLE = "Ethan's App";
	public static final int APP_WIDTH = 800;
	public static final String DEFAULT_LOCATION = "test1";

	public static void navigate(String location) {
		instance.doNavigate(location);
	}

	@Override
	public void init() {
		templateManager = new TemplateManager();

		templateManager.init();

		register(this);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = new Scene(new Group(), APP_WIDTH, APP_HEIGHT);

		primaryStage.setScene(scene);
		primaryStage.sizeToScene();

		primaryStage.setTitle(APP_TITLE);

		doNavigate(DEFAULT_LOCATION);

		primaryStage.show();
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	protected static void register(Application application) {
		instance = application;
	}

	protected void doNavigate(String location) {
		Scene scene = primaryStage.getScene();

		JSONObject template = templateManager.getTemplate(location);

		Region region = TemplateTranslator.getSceneContent(template);

		region.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
		region.setPrefSize(scene.getWidth(), scene.getHeight());

		scene.setRoot(region);
	}

	protected static Application instance;

	protected TemplateManager templateManager;
	protected Stage primaryStage;

}
