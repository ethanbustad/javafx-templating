/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.elements;

import com.ethanbustad.javafx.templating.actions.EventHandlerFactory;

import java.util.List;

import javafx.event.EventHandler;

import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import org.json.JSONObject;

public class Input extends javafx.scene.layout.Pane {

	public static enum Field {
		INITIAL_VALUE, INPUT_TYPE;

		public String value() {
			return toString.toLowerCase();
		}

	}

	public static enum InputType {
		CHECKBOX, DROPDOWN, PASSWORD, SUBMIT, TEXT, TEXTAREA;
	}

	public static final String INPUT_CSS_CLASS = "input";

	public static Input create(JSONObject inputTemplate) {
		Input input = new Input();

		input.getStyleClass().add(HEADER_CSS_CLASS);

		JSONObject location = inputTemplate.optJSONObject(
			NodeFactory.Field.LOCATION.value());

		if (location != null) {
			LocationUtil.setLocation(input, location);
		}

		List<Node> elements = input.getChildren();

		String text = inputTemplate.optString(
			NodeFactory.Field.TEXT.value());

		if ((text != null) && !text.isEmpty()) {
			Label label = new Label(text);

			elements.add(label);
		}

		Control controlElement = getControlElement(inputTemplate);

		elements.add(controlElement);

		return input;
	}

	protected Control getControlElement(JSONObject inputTemplate) {
		InputType inputType = null;

		String inputTypeString = inputTemplate.optString(
			Field.INPUT_TYPE.value(), InputType.TEXT.toString());

		try {
			inputType = Type.valueOf(inputTypeString.toUpperCase());
		}
		catch (IllegalArgumentException iae) {
			Logger.log("Input type \"" + inputTypeString + "\" is invalid.");

			return null;
		}

		if (inputType.equals(InputType.CHECKBOX)) {
			//
		}
		else if (inputType.equals(Type.DROPDOWN)) {
			//
		}
		else if (inputType.equals(Type.PASSWORD)) {
			return new PasswordField();
		}
		else if (inputType.equals(Type.SUBMIT)) {
			//
		}
		else if (inputType.equals(Type.TEXT)) {
			String initialValue = inputTemplate.optString(
				Field.INITIAL_VALUE.value(), "");

			return new TextField(initialValue);
		}
		else if (inputType.equals(Type.TEXTAREA)) {
			String initialValue = inputTemplate.optString(
				Field.INITIAL_VALUE.value(), "");

			return new TextArea(initialValue);
		}
		else {
			throw new ImpossibleException("I must have missed a conditional");
		}
	}

}
