/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.elements;

import com.ethanbustad.javafx.templating.actions.EventHandlerFactory;

import org.json.JSONObject;

public class Header extends javafx.scene.text.Text {

	public static final String HEADER_CSS_CLASS = "header";

	public static Header create(JSONObject headerTemplate) {
		Header header = new Header();

		header.getStyleClass().add(HEADER_CSS_CLASS);

		JSONObject location = headerTemplate.optJSONObject(
			NodeFactory.Field.LOCATION.value());

		if (location != null) {
			LocationUtil.setLocation(header, location);
		}

		String text = headerTemplate.optString(
			NodeFactory.Field.TEXT.value());

		if ((text != null) && !text.isEmpty()) {
			header.setText(text);
		}

		return header;
	}

}
