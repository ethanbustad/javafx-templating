/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.elements;

import javafx.scene.Node;
import javafx.scene.layout.GridPane;

import org.json.JSONObject;

public class LocationUtil {

	public static enum Field {
		COL, COLSPAN, ROW, ROWSPAN;

		public String value() {
			return toString().toLowerCase();
		}

	}

	public static void setLocation(Node node, JSONObject location) {
		GridPane.setConstraints(
			node, location.getInt(Field.COL.value()),
			location.getInt(Field.ROW.value()),
			location.getInt(Field.COLSPAN.value()),
			location.getInt(Field.ROWSPAN.value()));
	}

}
