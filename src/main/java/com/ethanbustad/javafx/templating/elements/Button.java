/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.elements;

import com.ethanbustad.javafx.templating.actions.EventHandlerFactory;

import javafx.event.EventHandler;

import org.json.JSONObject;

public class Button extends javafx.scene.control.Button {

	public static Button create(JSONObject buttonTemplate) {
		Button button = new Button();

		Object actionObject = buttonTemplate.opt(
			NodeFactory.Field.ACTION.value());

		if (actionObject != null) {
			EventHandler eventHandler = EventHandlerFactory.getEventHandler(
				actionObject);

			button.setOnAction(eventHandler);
		}

		JSONObject location = buttonTemplate.optJSONObject(
			NodeFactory.Field.LOCATION.value());

		if (location != null) {
			LocationUtil.setLocation(button, location);
		}

		String text = buttonTemplate.optString(
			NodeFactory.Field.TEXT.value());

		if ((text != null) && !text.isEmpty()) {
			button.setText(text);
		}

		return button;
	}

}
