/*
Copyright 2017 Ethan Bustad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ethanbustad.javafx.templating.elements;

import com.ethanbustad.javafx.templating.util.Logger;

import javafx.scene.Node;

import org.json.JSONObject;

public class NodeFactory {

	public static enum Field {
		ACTION, LOCATION, TEXT, TYPE;

		public String value() {
			return toString.toLowerCase();
		}

	}

	public static enum Type {
		BUTTON, HEADER, INPUT;
	}

	public static Node getNode(JSONObject template) {
		Type type = null;

		String typeString = template.optString(Field.TYPE.value());

		try {
			type = Type.valueOf(typeString.toUpperCase());
		}
		catch (IllegalArgumentException|NullPointerException e) {
			Logger.log("Node type \"" + typeString + "\" is invalid.");

			return null;
		}

		if (type.equals(Type.BUTTON)) {
			return Button.create(template);
		}
		else if (type.equals(Type.HEADER)) {
			return Header.create(template);
		}
		else if (type.equals(Type.INPUT)) {
			return Input.create(template);
		}
		else {
			throw new ImpossibleException("I must have missed a conditional");
		}
	}

}
