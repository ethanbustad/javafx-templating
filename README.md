# JavaFX Application Templating

Framework for building a JavaFX application more quickly, using JSON files.

## Java API

A simple Java API is provided for generating JavaFX pages from `.json` template files:

* JavaFX API wrappers available for extension:
  * `com/ethanbustad/javafx/templating/Application.java` - main class of the JavaFX application. This class can be used as-is, or can be simply subclassed, in which case the runtime should point to the subclass as the application's main class.
  * `com/ethanbustad/javafx/templating/actions/EventHandlerFactory.java` - instantiates JavaFX `EventHandler`s as needed from JSON content. The existing implementations can be used, or new implementations can be written and injected into the system by registering a new Factory (TODO).
  * `com/ethanbustad/javafx/templating/elements/NodeFactory.java` - instantiates JavaFX `Node`s as needed from JSON content. The existing implementations can be used, or new implementations can be written and injected into the system by registering a new Factory (TODO).
* Other extension points:
  * `com/ethanbustad/javafx/templating/util/Logger.java` - instantiates loggers that are used throughout the application. The existing implementations can be used, or new implementations can be written and injected into the system by registering a new Factory (TODO).

## Template `.json` files

### `template-files.json`

* This file serves as a registry of template files the system should recognize.
* This file is structured as a simple JSON object. Each entry consists of:
  * Key: A unique identifier string for the template file. This is how the template will be accessed by the application, using `TemplateManager.getTemplate(id)`.
  * Value: A relative path (starting in this directory) pointing to the location of the identified file. Paths should not start with a slash.
* Registered files are read on application startup and stored in memory.

### Template files

* These files are structured as nested JSON objects, storing all the necessary information to create a "page" on the application.
* Each file should store a single template JSON object.
* The allowable elements are as follows:
  * TBD

## License

Copyright 2017, Ethan Bustad.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
